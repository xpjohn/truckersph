<!-- Navbar -->
<nav class="navbar navbar-expand-md sticky-top navbar-light bg-light">
	<div class="container">
		<a class="navbar-brand" href="../admin/dashboard.php">
		<img src="../images/logo-trans.png" alt="Truckers Philippines Logo" height="50">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav ml-auto">
				<?php
				if($current_file_name == "dashboard.php" || $current_file_name == "blog.php" || $current_file_name == "application.php" || $current_file_name == "event.php"){
					echo '<li class="nav-item">
					<a class="nav-link" href="../admin/dashboard.php">Dashboard</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../admin/blog.php">Blog</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../admin/application.php">Application</a>
				</li>';
				}
				?>
				<li class="nav-item">
					<a class="nav-link" href="../admin/event.php">Event</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../logout.php">Sign out</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
