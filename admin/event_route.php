<?php
	session_start();
	include("../includes/dbcon.php");
	include("../includes/header.php");
	include("../admin/nav.php");
	if($_SESSION['status'] != "adminlogin"){ 
		header("Location: ../index.php"); 
	}
	if(isset($_POST['details'])){
		$game = $_POST["game"];
		//echo $game;
		if ($game=="ets2"){
			$mysqli->set_charset('utf8');
			$dlc_base = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Base Game' ORDER BY city");
			$dlc_goingeast = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Going East' ORDER BY city");
			$dlc_scandinavia = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Scandinavia' ORDER BY city");
			$dlc_vive = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Vive la France!' ORDER BY city");
			$dlc_italia = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Italia' ORDER BY city");

			$d_dlc_base = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Base Game' ORDER BY city");
			$d_dlc_goingeast = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Going East' ORDER BY city");
			$d_dlc_scandinavia = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Scandinavia' ORDER BY city");
			$d_dlc_vive = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Vive la France!' ORDER BY city");
			$d_dlc_italia = $mysqli->query("SELECT * FROM tbl_ets2place WHERE dlc='Italia' ORDER BY city");
		}
		else if ($game="ats"){
			$mysqli->set_charset('utf8');
			$dlc_base = $mysqli->query("SELECT * FROM tbl_atsplace WHERE dlc='Base Game' ORDER BY city");
			$dlc_arizona = $mysqli->query("SELECT * FROM tbl_atsplace WHERE dlc='Arizona' ORDER BY city");
			$dlc_newmexico = $mysqli->query("SELECT * FROM tbl_atsplace WHERE dlc='New Mexico' ORDER BY city");

			$d_dlc_base = $mysqli->query("SELECT * FROM tbl_atsplace WHERE dlc='Base Game' ORDER BY city");
			$d_dlc_arizona = $mysqli->query("SELECT * FROM tbl_atsplace WHERE dlc='Arizona' ORDER BY city");
			$d_dlc_newmexico = $mysqli->query("SELECT * FROM tbl_atsplace WHERE dlc='New Mexico' ORDER BY city");
		}
	}
?>
<div class="container"><br>
	<h1 style="text-align:center">Route</h1><br>
	<div class="row">
		<div class="col-md"></div>
		<div class="col-md-6">
			<form action="../admin/event_map.php" method="post">
				<div class="form-group row">
					<label for="server" class="col-sm-3 col-form-label">Origin</label>
					<div class="col-sm-9">
						<select class="form-control">
							<?php if($game=="ets2"){ ?>
								<optgroup label="Base Game">
									<?php
										while($row = $dlc_base->fetch_assoc()){
											$ocity = $row['city'];
											$ocountry = $row['country'];
											echo "<option value=\"$ocity, $ocountry\">$ocity, $ocountry</option>";
										}
										?>
								</optgroup>
								<optgroup label="Going East">
									<?php
										while($row = $dlc_goingeast->fetch_assoc()){
											$ocity = $row['city'];
											$ocountry = $row['country'];
											echo "<option value=\"$ocity, $ocountry\">$ocity, $ocountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="Scandinavia">
									<?php
										while($row = $dlc_scandinavia->fetch_assoc()){
											$ocity = $row['city'];
											$ocountry = $row['country'];
											echo "<option value=\"$ocity, $ocountry\">$ocity, $ocountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="Vive La France!">
									<?php
										while($row = $dlc_vive->fetch_assoc()){
											$ocity = $row['city'];
											$ocountry = $row['country'];
											echo "<option value=\"$ocity, $ocountry\">$ocity, $ocountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="Italia">
									<?php
										while($row = $dlc_italia->fetch_assoc()){
											$ocity = $row['city'];
											$ocountry = $row['country'];
											echo "<option value=\"$ocity, $ocountry\">$ocity, $ocountry</option>";
										}
									?>
								</optgroup>
							<?php } else if($game=="ats"){ ?>
								<optgroup label="Base Game">
									<?php
										while($row = $dlc_base->fetch_assoc()){
											$ocity = $row['city'];
											$ocountry = $row['country'];
											echo "<option value=\"$ocity, $ocountry\">$ocity, $ocountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="Arizona">
									<?php
										while($row = $dlc_arizona->fetch_assoc()){
											$ocity = $row['city'];
											$ocountry = $row['country'];
											echo "<option value=\"$ocity, $ocountry\">$ocity, $ocountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="New Mexico">
									<?php
										while($row = $dlc_newmexico->fetch_assoc()){
											$ocity = $row['city'];
											$ocountry = $row['country'];
											echo "<option value=\"$ocity, $ocountry\">$ocity, $ocountry</option>";
										}
									?>
								</optgroup>
							<?php } ?>
						</select>
					</div>
				</div><!-- End of Origin -->
				<div class="form-group row">
					<label for="server" class="col-sm-3 col-form-label">Destination</label>
					<div class="col-sm-9">
						<select class="form-control">
							<?php if($game=="ets2"){ ?>
								<optgroup label="Base Game">
									<?php
										while($row = $d_dlc_base->fetch_assoc()){
											$dcity = $row['city'];
											$dcountry = $row['country'];
											echo "<option value=\"$dcity, $dcountry\">$dcity, $dcountry</option>";
										}
										?>
								</optgroup>
								<optgroup label="Going East">
									<?php
										while($row = $d_dlc_goingeast->fetch_assoc()){
											$dcity = $row['city'];
											$dcountry = $row['country'];
											echo "<option value=\"$dcity, $dcountry\">$dcity, $dcountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="Scandinavia">
									<?php
										while($row = $d_dlc_scandinavia->fetch_assoc()){
											$dcity = $row['city'];
											$dcountry = $row['country'];
											echo "<option value=\"$dcity, $dcountry\">$dcity, $dcountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="Vive La France!">
									<?php
										while($row = $d_dlc_vive->fetch_assoc()){
											$dcity = $row['city'];
											$dcountry = $row['country'];
											echo "<option value=\"$dcity, $dcountry\">$dcity, $dcountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="Italia">
									<?php
										while($row = $d_dlc_italia->fetch_assoc()){
											$dcity = $row['city'];
											$dcountry = $row['country'];
											echo "<option value=\"$dcity, $dcountry\">$dcity, $dcountry</option>";
										}
									?>
								</optgroup>
							<?php } else if($game=="ats"){ ?>
								<optgroup label="Base Game">
									<?php
										while($row = $d_dlc_base->fetch_assoc()){
											$dcity = $row['city'];
											$dcountry = $row['country'];
											echo "<option value=\"$dcity, $dcountry\">$dcity, $dcountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="Arizona">
									<?php
										while($row = $d_dlc_arizona->fetch_assoc()){
											$dcity = $row['city'];
											$dcountry = $row['country'];
											echo "<option value=\"$dcity, $dcountry\">$dcity, $dcountry</option>";
										}
									?>
								</optgroup>
								<optgroup label="New Mexico">
									<?php
										while($row = $d_dlc_newmexico->fetch_assoc()){
											$dcity = $row['city'];
											$dcountry = $row['country'];
											echo "<option value=\"$dcity, $dcountry\">$dcity, $dcountry</option>";
										}
									?>
								</optgroup>
							<?php } ?>	
						</select>
					</div>
				</div><!-- End of Desination -->
				<div class="form-group row">
					<label for="server" class="col-sm-3 col-form-label">Trip Length*</label>
					<div class="col-sm-9">
						<input type="number" class="form-control" name="length">
					</div>
				</div>
				<div class="form-group row">
					<label for="server" class="col-sm-3 col-form-label">Cargo</label>
				</div>
				<div class="form-group row">
					<label for="server" class="col-sm-3 col-form-label">Weight*</label>
					<div class="col-sm-9">
						<input type="number" class="form-control" name="weight">
					</div>
				</div>
				<div class="form-group row">
					<label for="server" class="col-sm-3 col-form-label">Alternate Cargo</label>
				</div>
				<div class="form-group row">
					<label for="server" class="col-sm-3 col-form-label">Trailer</label>
				</div>
				<div class="form-group row">
					<div class="col-sm-9 offset-md-3">
						<a href="../admin/event_details.php" class="btn btn-danger" role="button" aria-pressed="true">Previous</a>
						<button type="submit" class="btn btn-success" name="route">Next</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md"></div>
	</div>
</div>
<?php
	include("footer.php");
	include("../includes/end.php"); 
?>