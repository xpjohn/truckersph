<?php
	include("../includes/dbcon.php");
	include("../includes/header.php");
	include("../admin/nav.php")
?>
<div class="container">
<br>
	<div>
		<nav class="navbar justify-content-between">
			<h1> Events </h1>
			<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post" class="form-inline">
				<a href="../admin/event_add.php" class="btn btn-primary mr-sm-2 my-2 my-sm-0" role="button" aria-pressed="true">Add Event</a>
				<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
		</nav>
	</div>

</div>
<?php
	include("footer.php");
	include("../includes/end.php"); 
?>