<?php
	session_start();
	include("../includes/dbcon.php");
	include("../includes/header.php");
	include("../admin/nav.php");
	if($_SESSION['status'] != "adminlogin"){ 
		header("Location: ../index.php"); 
	}
?>
<div class="container"><br>
	<div class="row">
		<div class="col-md"></div>
		<div class="col-md-6" style="text-align:center">
			<h1>Game Selection</h1><br>
			<form action="../admin/event_details.php" method="post">
				<div class="form-group">
					<img src="../images/ets2-logo.png" height="100px">
					<div class="form-check">
						<input class="form-check-input" type="radio" name="game" value="ets2" required>
						<label class="form-check-label" for="ets2">Euro Truck Simulator 2</label>
					</div>
				</div>
				<div class="form-group">
					<img src="../images/ats-logo.png" height="100px">
					<div class="form-check">
						<input class="form-check-input" type="radio" name="game" value="ats" required>
						<label class="form-check-label" for="ats">American Truck Simulator</label>
					</div>
				</div>
				<div class="form-group">
					<a href="../admin/event.php" class="btn btn-danger" role="button" aria-pressed="true">Previous</a>
					<button type="submit" class="btn btn-success" name="gameselect">Next</button>
				</div>
			</form>
		</div>
		<div class="col-md"></div>
	</div>
</div>
<?php
	include("footer.php");
	include("../includes/end.php"); 
?>