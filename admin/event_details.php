<?php
	session_start();
	include("../includes/dbcon.php");
	include("../includes/header.php");
	include("../admin/nav.php");
	if($_SESSION['status'] != "adminlogin"){
		header("Location: ../index.php");
	}
	if(isset($_POST['gameselect'])){
		$game = $_POST["game"];
		//echo $game;
		if ($game=="ets2"){
			$server = array(
				"ETS2 EU#1 (Simulation)",
				"ETS2 EU#2",
				"ETS2 EU#3 (No Cars)",
				"ETS2 EU#4 (Freeroam, No Collision)",
				"ETS2 US",
				"ETS2 SA"
			);
		}
		else if ($game="ats"){
			$server = array(
				"ATS EU"
			);
		}
	}
?>
<div class="container"><br>
	<h1 style="text-align:center">Event Details</h1><br>
	<div class="row">
		<div class="col-md"></div>
		<div class="col-md-6" >
			<form action="../admin/event_route.php" method="post">
			<input type="hidden" name="game" value="<?php echo $game;?>">
				<div class="form-group row">
					<label for="" class="col-sm-3 col-form-label">Event Title</label>
					<div class="col-sm-9">
						<input type="text" name="title" class="form-control" placeholder="">
					</div>
				</div>
				<div class="form-group row">
					<label for="server" class="col-sm-3 col-form-label">Server</label>
					<div class="col-sm-9">
						<select class="form-control">
							<option value="private">Private</option>
							<option value="private">Public</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="type" class="col-sm-3 col-form-label">Date</label>
					<div class="col-sm-9">
						<input type="date" name"date" class="form-control" placeholder="">
					</div>
				</div>
				<div class="form-group row">
					<label for="type" class="col-sm-3 col-form-label">Meet-up Time</label>
					<div class="col-sm-9">
						<input type="time" name"mdate" class="form-control" placeholder="">
					</div>
				</div>
				<div class="form-group row">
					<label for="type" class="col-sm-3 col-form-label">Departure Time</label>
					<div class="col-sm-9">
						<input type="time" name"ddate" class="form-control" placeholder="">
					</div>
				</div>
				<div class="form-group row">
					<label for="server" class="col-sm-3 col-form-label">Server</label>
					<div class="col-sm-9">
						<select class="form-control">
							<?php
								for ($i=0; $i<count($server); $i++){
									echo "<option value=\"$server[$i]\">$server[$i]</option>";
								}
							?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-9 offset-md-3">
						<a href="../admin/event_add.php" class="btn btn-danger" role="button" aria-pressed="true">Previous</a>
						<button type="submit" class="btn btn-success" name="details" >Next</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md"></div>
	</div>
</div>
<?php
	include("footer.php");
	include("../includes/end.php"); 
?>