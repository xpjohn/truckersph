<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" type="image/png" href=""/>
	<title>Truckers Philippines</title>

	<!-- PHP Scripts -->
	<?php
		$current_file_name = basename($_SERVER['PHP_SELF']);
	?>

	<!-- Bootstrap -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!-- Custom CSS -->
	<?php
	if($current_file_name == "dashboard.php" || $current_file_name == "blog.php" || $current_file_name == "application.php" || $current_file_name == "event.php" || $current_file_name == "event_add.php" || $current_file_name == "event_route.php" || $current_file_name == "event_details.php"){
		echo '<link href="../css/admin.css" rel="stylesheet" type="text/css">';
	}
	if($current_file_name == "login.php"){
		echo '<style>
		.center {
			display: block;
			margin-left: auto;
			margin-right: auto;
			padding-top: 50px;
			padding-bottom: 50px;
		}
		</style>';
	}?>
	<!-- Fonts -->
	<link href="" rel="stylesheet" type="text/css">
</head>
<body>
