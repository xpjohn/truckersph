<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Truckers PH Events</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<?php
		include("array.php");
		?>
		<div class="container-fluid">
			<div id="header">
				<h1 style="text-align: center;"> Truckers Philippines Events v1.4</h1>
			</div>
			<div id="form-body" class="row">
				<form class="form-horizontal" action="rcv.php" method="post">
					<div class="col-md-1"></div>
					<div class="col-md-5">
						<div class="form-group">
							<label for="title" class="col-md-4 control-label">Description</label>
							<div class="col-md-8">
								<input class="form-control" type="text" placeholder="Saturday Night Convoy" name="title">
							</div>
						</div>
						<div class="form-group">
							<label for="date" class="col-md-4 control-label">Date (yyMMdd)</label>
							<div class="col-md-8">
								<input class="form-control" type="number" placeholder="171231" name="date" required>
							</div>
						</div>
						<div class="form-group">
							<label for="server" class="col-md-4 control-label">Server</label>
							<div class="col-md-8">
								<select class="form-control" name="server">
									<optgroup label="European Truck Simulator 2">
										<option id="ets2" value="ETS2 EU#1">ETS2 Europe #1</option>
										<option id="ets2" value="ETS2 EU#2">ETS2 Europe #2</option>
										<option id="ets2" value="ETS2 EU-Trial">ETS2 Europe [Trial Test Rules]</option>
										<option id="ets2" value="ETS2 US">ETS2 Unites States</option>
										<option id="ets2" value="ETS2 SA">ETS2 South America</option>
										<option id="ets2" value="ETS2 HK">ETS2 Hongkong</option>
									</optgroup>
									<optgroup label="American Truck Simulator">
										<option id="ats" value="ATS US#1">ATS United States</option>
										<option id="ats" value="ATS EU#2">ATS Europe #2</option>
									</optgroup>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="type" class="col-md-4 control-label">Convoy Type</label>
							<div class="col-md-8">
								<select class="form-control" name="type">
									<option value="Public">Public</option>
									<option value="Private">Private</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="meetup" class="col-md-4 control-label">Meet-up Time (hhmm)</label>
							<div class="col-md-8">
								<input class="form-control" type="number" max="2359" placeholder="2030" name="meetup" required title="4 characters minimum">
							</div>
						</div>
						<div class="form-group">
							<label for="departure" class="col-md-4 control-label">Departure Time (hhmm)</label>
							<div class="col-md-8">
								<input class="form-control" type="number" max="2359"  placeholder="2100" name="departure" required title="4 characters minimum">
							</div>
						</div>
						<!-- <div class="form-group">
							<label for="type" class="col-md-4 control-label">Meet-up Time</label>
							<div class="input-group date col-md-8" id="datetimepicker1">
								<input type='text' class="form-control" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
							</div>
							<div class="form-group">
							<label for="type" class="col-md-4 control-label">Departure Time</label>
							<div class="input-group date col-md-8" id="datetimepicker1">
								<input type='text' class="form-control" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
							</div> -->
						
						<div class="form-group">
							<label for="origin" class="col-md-4 control-label">Origin & Company</label>
							<div class="col-md-4">
								<select class="form-control" type="text" placeholder="Manila, Philippines" name="origin" list="cities" required>
									<datalist id="cities">
										<optgroup label="Euro Truck Simulator 2">
											<?php
												for ($i=0; $i<count($ets2cities); $i++){
													echo "<option value=\"$ets2cities[$i]\">$ets2cities[$i]</option>";
												}
												?>
										</optgroup>
										<optgroup label="American Truck Simulator">
											<?php
												for ($i=0; $i<count($atscities); $i++){
													echo "<option value=\"$atscities[$i]\">$atscities[$i]</option>";
												}
												?>
										</optgroup>
									</datalist>
								</select>
							</div>
							<div class="col-md-4">
								<select class="form-control" type="text" placeholder="Agronord" name="origincompany" list="companies" required>
									<datalist id="companies">
										<optgroup label="Euro Truck Simulator 2">
											<?php
												for ($i=0; $i<count($ets2companies); $i++){
													echo "<option value=\"$ets2companies[$i]\">$ets2companies[$i]</option>";
												}
												?>
										</optgroup>
										<optgroup label="American Truck Simulator">
											<?php
												for ($i=0; $i<count($atscompanies); $i++){
													echo "<option value=\"$atscompanies[$i]\">$atscompanies[$i]</option>";
												}
												?>
										</optgroup>
									</datalist>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="destination" class="col-md-4 control-label">Destination & Company</label>
							<div class="col-md-4">
								<select class="form-control" type="text" placeholder="Manila, Philippines" name="destination" list="cities" required>
									<datalist id="cities">
										<optgroup label="Euro Truck Simulator 2">
											<?php
												for ($i=0; $i<count($ets2cities); $i++){
													echo "<option value=\"$ets2cities[$i]\">$ets2cities[$i]</option>";
												}
												?>
										</optgroup>
										<optgroup label="American Truck Simulator">
											<?php
												for ($i=0; $i<count($atscities); $i++){
													echo "<option value=\"$atscities[$i]\">$atscities[$i]</option>";
												}
												?>
										</optgroup>
									</datalist>
								</select>
							</div>
							<div class="col-md-4">
								<select class="form-control" type="text" placeholder="Agronord" name="destinationcompany" list="companies" required>
									<datalist id="companies">
										<optgroup label="Euro Truck Simulator 2">
											<?php
												for ($i=0; $i<count($ets2companies); $i++){
													echo "<option value=\"$ets2companies[$i]\">$ets2companies[$i]</option>";
												}
												?>
										</optgroup>
										<optgroup label="American Truck Simulator">
											<?php
												for ($i=0; $i<count($atscompanies); $i++){
													echo "<option value=\"$atscompanies[$i]\">$atscompanies[$i]</option>";
												}
												?>
										</optgroup>
									</datalist>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="cargo" class="col-md-4 control-label">Cargo & Weight</label>
							<div class="col-md-5">
								<input class="form-control" type="text" placeholder="Locomotive" name="cargo1" required>
							</div>
							<div class="col-md-3">
								<input class="form-control" type="number" placeholder="25" name="weight1" required>
							</div>
						</div>
						<div class="form-group">
							<label for="cargo" class="col-md-4 control-label">Alternative Cargo & Weight</label>
							<div class="col-md-5">
								<input class="form-control" type="text" placeholder="Locomotive" name="cargo2">
							</div>
							<div class="col-md-3">
								<input class="form-control" type="number" placeholder="25" name="weight2">
							</div>
						</div>

						<div class="form-group">
							<label for="trailer" class="col-md-4 control-label">Trailer Required</label>
							<div class="col-md-8">
								<select class="form-control" type="text" name="trailer" list="trailer" required>
									<option value="none">none</option>
									<datalist id="companies">
										<optgroup label="Euro Truck Simulator 2">
											<?php
												for ($i=0; $i<count($ets2trailers); $i++){
													echo "<option value=\"$ets2trailers[$i]\">$ets2trailers[$i]</option>";
												}
												?>
										</optgroup>
										<optgroup label="American Truck Simulator">
											<?php
												for ($i=0; $i<count($atstrailers); $i++){
													echo "<option value=\"$atstrailers[$i]\">$atstrailers[$i]</option>";
												}
												?>
										</optgroup>
									</datalist>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="km" class="col-md-4 control-label">Trip Length</label>
							<div class="col-md-8">
								<input class="form-control" type="number" placeholder="2500" name="km" required>
							</div>
						</div>
						<div class="form-group form-check">
							<label for="dlc" class="col-md-4 control-label form-check-label">DLC Required</label>
							<div class="col-md-8 alldisabled">
								<div class="form-check none">
									<label class="form-check-label">
									<input type="checkbox" class="form-check-input" name="dlc[]" value="none">
									none
									</label>
								</div>
								<?php
									for($i=0; $i<count($dlc); $i++){
										echo '<div class="form-check">';
										echo '<label class="form-check-label">';
										echo '<input type="checkbox" class="form-check-input" name="dlc[]" value="'. $dlc[$i] .'"> ';
										echo $dlc[$i];
										echo '</label>';
										echo '</div>';
									}
									?>
							</div>
						</div>
						<div class="form-group form-check">
							<label for="skills" class="col-md-4 control-label form-check-label">Skills Required</label>
							<div class="col-md-8 alldisabled">
								<div class="form-check none">
									<label class="form-check-label">
									<input type="checkbox" class="form-check-input" name="skills[]" value="none">
									none
									</label>
								</div>
								<?php
									for($i=0; $i<count($skills); $i++){
										echo '<div class="form-check">';
										echo '<label class="form-check-label">';
										echo '<input type="checkbox" class="form-check-input" name="skills[]" value="'. $skills[$i] .'"> ';
										echo $skills[$i];
										echo '</label>';
										echo '</div>';
									}
									for($i=0; $i<count($skillsadr); $i++){
										echo '<div class="form-check adrnone">';
										echo '<label class="form-check-label">';
										echo '<input type="checkbox" class="form-check-input" name="skills[]" value="'. $skillsadr[$i] .'"> ';
										echo $skillsadr[$i];
										echo '</label>';
										echo '</div>';
									}
									?>
							</div>
						</div>
					</div>
					<div class="col-md-5">
					<div class="form-group">
							<label for="truck" class="col-md-4 control-label">Truck Required</label>
							<div class="col-md-8">
								<select class="form-control" type="text" name="truck" list="truck" required>
									<option value="none">none</option>
									<datalist id="truck">
										<?php
											for ($i=0; $i<count($ets2trucks); $i++){
												echo "<option value=\"$ets2trucks[$i]\">$ets2trucks[$i]</option>";
											}
											?>
									</datalist>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="chassis" class="col-md-4 control-label">Chassis</label>
							<div class="col-md-8">
								<select class="form-control" type="text" name="chassis" required>
								<option value="none">none</option>
								<?php
									for ($i=0; $i<count($ets2chassis); $i++){
										echo "<option value=\"$ets2chassis[$i]\">$ets2chassis[$i]</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="paint" class="col-md-4 control-label">Paint Job</label>
							<div class="col-md-8">
								<input class="form-control" type="text" placeholder="Sunrise" name="paint">
							</div>
						</div>
						<div class="form-group">
							<label for="point" class="col-md-4 control-label">Route</label>
							<div class="col-md-8">
								<select class="form-control custom" name="route">
									<option value="default">Default GPS Route</option>
									<option value="custom">Custom Route</option>
								</select>
							</div>
						</div>
						<!-- <div class="form-group">
							<label for="point" class="col-md-4 control-label">Point #1</label>
							<input type="checkbox" class="form-check-input" name="point[]" value="none">
							<div class="col-md-3">
								<input class="form-control nocustom" type="text" placeholder="Place" name="point[]" required>
							</div>
							<div class="col-md-2">
								<select class="form-control direction nocustom" name="direction">
									<option value="none">none</option>
									<option value="near">near</option>
									<option value="North of">north of</option>
									<option value="South of">south of</option>
									<option value="East of">east of</option>
									<option value="West of">west of</option>
								</select>
							</div>
							<div class="col-md-3">
								<input class="form-control nodirection nocustom" type="text" placeholder="City" name="nearlocation[]" required>
							</div>
						</div> -->
						<?php
						for ($i=1; $i<=10; $i++){
							echo '<div class="form-group">';
							echo '<label for="point" class="col-md-4 control-label">Point #' .$i. '</label>';
							echo '<!-- <input type="checkbox" class="form-check-input" name="point[]" value="none"> -->';
							echo '<div class="col-md-3">';
							echo '<input class="form-control nocustom" type="text" placeholder="Place" name="point[]">';
							echo '</div>';
							echo '<div class="col-md-2">';
							echo '<select class="form-control direction'.$i.' nocustom" name="direction[]">';
							echo '<option value="none">none</option>';
							echo '<option value="near">near</option>';
							echo '<option value="north of">north of</option>';
							echo '<option value="south of">south of</option>';
							echo '<option value="east of">east of</option>';
							echo '<option value="west of">west of</option>';
							echo '</select>';
							echo '</div>';
							echo '<div class="col-md-3">';
							echo '<select class="form-control nocustom" type="text" name="nearlocation[]" list="cities" required>';
							echo '<datalist id="cities">';
							echo '<optgroup label="Euro Truck Simulator 2">';
							for ($j=0; $j<count($ets2cities); $j++){
							echo "<option value=\"$ets2cities[$j]\">$ets2cities[$j]</option>";
							}
							echo '</optgroup>';
							echo '<optgroup label="American Truck Simulator">';
							for ($j=0; $j<count($atscities); $j++){
							echo "<option value=\"$atscities[$j]\">$atscities[$j]</option>";
							}
							echo '</optgroup>';
							echo '</datalist>';
							echo '</select>';
							echo '</div>';
							echo '</div>';
						}
						
						?>
						
						<div class="form-group">
							<label for="stopover" class="col-md-4 control-label">Stopover</label>
							<div class="col-md-8">
								<select class="form-control nocustom" type="text" placeholder="Manila, Philippines" name="stopover" list="cities" required>
									<option value="none">none</option>
									<?php
										for ($i=1; $i<=10; $i++){
											echo "<option value=\"stopover$i\">Point #$i</option>";
										}
										?>
								</select>
							</div>
						</div>


						<!-- Submit -->
						<div class="form-group">
							<div class="col-md-8 col-md-push-4">
								<input type="submit" class="btn btn-primary" value="Proceed"/>
							</div>
						</div>
					</div>
					<div class="col-md-1"></div>
				</form>
			</div>
		</div>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-3.2.1.js"></script>
		<script src="js/js.js"></script>
	</body>
</html>