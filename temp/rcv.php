<!DOCTYPE html>
<html lang="en">
	<head>
        <title>Truckers PH Events</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-3.2.1.js"></script>
	</head>
	
	<body>
        <div class="container-fluid">
            <?php
            error_reporting(0);
            include("call.php");

            //Discord
            echo "<h1> Discord (#schedules)</h1>";
            //Part 1
            echo "<h3> Part $part (Insert Route Map)</h3>";
            echo "<pre>";
            echo "**$date Truckers Philippines Convoy ($titleorigin to $titledestination)**<br>";
            if (!empty($_POST["title"])){
                echo "*$title*<br>";
            }
            echo "<br>";
            echo "Server : $server<br>";
            echo "Convoy Type : $type<br>";
            echo "Meet-up Time : $meetup PHT / $utcm UTC";
            if ($bdatem != ""){
                echo " (".$bdatem.")";
            }
            echo "<br>";
            echo "Departure Time : $departure PHT / $utcd UTC";
            if ($bdated != ""){
                echo " (".$bdated.")";
            }
            echo "<br><br>";
            echo "Origin : $origin ($origincompany)<br>";
            echo "Destination : $destination ($destinationcompany)<br>";
            echo "Cargo : $cargo1 (" .$weight1. "t)<br>";
            if (!empty($cargo2)){
                echo "Alternative Cargo : $cargo2 (".$weight2."t)<br>";
            }
            echo "Stopover : ";
            if ($route == "custom"){
                for ($i=0; $i<10; $i++){
                    $c = $i + 1;
                    if ($stopover == "stopover$c"){
                        if (!empty($point[$i])){
                            echo "via $point[$i]";
                            if ($direction[$i] != "none"){
                                echo ", $direction[$i] $nearlocation[$i]";
                            }
                        }
                        else {
                            echo "none";
                        }
                    }
                }
            }
            else {
                echo "none";
            }
            echo "<br><br>";
            echo "Trip Length : " .$km. "km<br>";
            echo "Skill Required : ";
            echo implode(", ",$skills). "<br>";
            echo "DLC Required : ";
            echo implode(", ",$dlc);
            echo "<br><br>";
            echo "Route : $routecustom<br>";
            if ($route == "custom"){
                for ($i=0; $i<count($point); $i++){
                    if (!empty($point[$i])){
                        $pointcount = $pointcount + 1;
                        echo "**Point $pointcount** - via $point[$i]";
                        if ($direction[$i] != "none"){
                            echo ", *$direction[$i]* ***$nearlocation[$i]***";
                        }
                        echo "<br>";
                    }
                }
            }
            echo "<br>";
            echo "Route Map : <br>";
            echo "</pre>";
            $part++;
            //Part 2
            if ($truck != "none"){
                echo "<h3> Part $part (Insert Truck Image)</h3>";
                echo "<pre>";
                echo "Truck Required : $truck<br>";
                if ($chassis != "none"){
                    echo "Chassis : $chassis<br>";
                }
                if (!empty($paint)){
                    echo "Paint Job : $paint<br>";
                }
                echo "</pre>";
                $part++;
            }
            //Part 3
            if ($trailer != "none"){
                echo "<h3> Part $part (Insert Trailer Image)</h3>";
                echo "<pre>";
                echo "Trailer Required : $trailer<br>";
                echo "</pre>";
                $part++;
            }
            //Part 4
            echo "<h3> Part $part (Insert Truck & Trailer Image)</h3>";
            echo "<pre>";
            echo "Truck & Trailer : <br>";
            echo "</pre>";
            echo "<br><br>";


            //Steam
            echo "<h1> Steam </h1>";
            //Part 1
            echo "<h3> Event Name </h3>";
            echo "<pre>";
            echo "$date Truckers Philippines Convoy ($titleorigin to $titledestination)";
            echo "</pre>";
            //Part 2
            echo "<h3> Description </h3>";
            echo "<pre>";
            if (!empty($_POST["title"])){
                echo "$title<br><br>";
            }
            echo "Server : $server<br>";
            echo "Convoy Type : $type<br>";
            echo "Voice Chat : discord.gg/9nS6ksC<br>";
            echo "Meet-up Time : $meetup PHT / $utcm UTC";
            if ($bdatem != ""){
                echo " (".$bdatem.")";
            }
            echo "<br>";
            echo "Departure Time : $departure PHT / $utcd UTC";
            if ($bdated != ""){
                echo " (".$bdated.")";
            }
            echo "<br><br>";
            echo "Origin : $origin ($origincompany)<br>";
            echo "Destination : $destination ($destinationcompany)<br>";
            echo "Cargo : $cargo1 (" .$weight1. "t)<br>";
            if (!empty($cargo2)){
                echo "Alternative Cargo : $cargo2 (".$weight2."t)<br>";
            }
            echo "Stopover : ";
            if ($route == "custom"){
                for ($i=0; $i<10; $i++){
                    $c = $i + 1;
                    if ($stopover == "stopover$c"){
                        if (!empty($point[$i])){
                            echo "via $point[$i]";
                            if ($direction[$i] != "none"){
                                echo ", $direction[$i] $nearlocation[$i]";
                            }
                        }
                        else {
                            echo "none";
                        }
                    }
                }
            }
            else {
                echo "none";
            }
            echo "<br><br>";
            echo "Trip Length : " .$km. "km<br>";
            echo "Skill Required : ";
            echo implode(", ",$skills). "<br>";
            echo "DLC Required : ";
            echo implode(", ",$dlc);
            echo "<br><br>";
            echo "Route : $routecustom<br>";
            if ($route == "custom"){
                $pointcount = 0;
                for ($i=0; $i<count($point); $i++){
                    if (!empty($point[$i])){
                        $pointcount = $pointcount + 1;
                        echo "Point $pointcount - via $point[$i]";
                        if ($direction[$i] != "none"){
                            echo ", $direction[$i] $nearlocation[$i]";
                        }
                        echo "<br>";
                    }
                }
            }
            echo "<br>";
            if ($truck != "none"){
                echo "Truck Required : $truck<br>";
                if ($chassis != "none"){
                    echo "Chassis : $chassis<br>";
                }
                if (!empty($paint)){
                    echo "Paint Job : $paint<br>";
                }
                $part++;
            }
            if ($trailer != "none"){
                echo "Trailer Required : $trailer<br>";
                $part++;
            }
            echo "</pre>";
            echo "<br><br>";


            //Facebook
            echo "<h1> Facebook </h1>";
            echo "<pre>";
            echo "$date Truckers Philippines Convoy ($titleorigin to $titledestination)<br>";
            if (!empty($_POST["title"])){
                echo "$title<br><br>";
            }
            echo "Server : $server<br>";
            echo "Convoy Type : $type<br>";
            echo "Voice Chat : discord.gg/9nS6ksC<br>";
            echo "Meet-up Time : $meetup PHT / $utcm UTC";
            if ($bdatem != ""){
                echo " (".$bdatem.")";
            }
            echo "<br>";
            echo "Departure Time : $departure PHT / $utcd UTC";
            if ($bdated != ""){
                echo " (".$bdated.")";
            }
            echo "<br><br>";
            echo "Origin : $origin ($origincompany)<br>";
            echo "Destination : $destination ($destinationcompany)<br>";
            echo "Cargo : $cargo1 (" .$weight1. "t)<br>";
            if (!empty($cargo2)){
                echo "Alternative Cargo : $cargo2 (".$weight2."t)<br>";
            }
            echo "Stopover : ";
            if ($route == "custom"){
                for ($i=0; $i<10; $i++){
                    $c = $i + 1;
                    if ($stopover == "stopover$c"){
                        if (!empty($point[$i])){
                            echo "via $point[$i]";
                            if ($direction[$i] != "none"){
                                echo ", $direction[$i] $nearlocation[$i]";
                            }
                        }
                        else {
                            echo "none";
                        }
                    }
                }
            }
            else {
                echo "none";
            }
            echo "<br><br>";
            echo "Trip Length : " .$km. "km<br>";
            echo "Skill Required : ";
            echo implode(", ",$skills). "<br>";
            echo "DLC Required : ";
            echo implode(", ",$dlc);
            echo "<br><br>";
            echo "Route : $routecustom<br>";
            if ($route == "custom"){
                $pointcount = 0;
                for ($i=0; $i<count($point); $i++){
                    if (!empty($point[$i])){
                        $pointcount = $pointcount + 1;
                        echo "Point $pointcount - via $point[$i]";
                        if ($direction[$i] != "none"){
                            echo ", $direction[$i] $nearlocation[$i]";
                        }
                        echo "<br>";
                    }
                }
            }
            echo "<br>";
            if ($truck != "none"){
                echo "Truck Required : $truck<br>";
                if ($chassis != "none"){
                    echo "Chassis : $chassis<br>";
                }
                if (!empty($paint)){
                    echo "Paint Job : $paint<br>";
                }
                $part++;
            }
            if ($trailer != "none"){
                echo "Trailer Required : $trailer<br>";
                $part++;
            }
            echo "</pre>";
            echo "<br><br>";

            //ETS2C.com
            echo "<h1> ETS2C.com </h1>";
            echo "<br><br>";
            ?>
        </div>
    </body>
</html>