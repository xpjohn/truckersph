<?php
	session_start();
	include("includes/dbcon.php");
	include("includes/header.php");
	if(isset($_POST['login'])){
		$email = mysqli_real_escape_string($mysqli, $_POST['email']);
		$password = mysqli_real_escape_string($mysqli, $_POST['password']);
		//Error handlers
		//Check if inputs are empty
		if(empty($email) || empty($password)){
			$loginErr = "Incorrect login!";
			$err = 1;
		}
		else {
			$sql = "SELECT * FROM tbl_admin WHERE username = '$email'";
			$result = mysqli_query($mysqli, $sql);
			$admin_count = mysqli_num_rows($result);
			if($admin_count > 0){
				if($row = mysqli_fetch_assoc($result)){
					//$hasedPwdCheck = password_verify($password, $row['password']);
					if($password == $row['password']){
						include("session_admin.php");
						header("Location: admin/dashboard.php");
					}
					else{
						header("Location: login.php");
					}
				}
			}
			else {
				$loginErr = "Incorrect login!";
				$err = 1;
			}
		}
	}
?>
<div class="container">
	<div id="header">
		<div class="headerlogo">
			<a href="index.php">
			<img src="images/logo-trans.png" height="200px" class="center">
			</a>
		</div>
	</div>
	<div id="form-body">
		<h2 style="text-align:center;"> Sign in </h2>
		<br>
		<form class="form-horizontal" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
			<div class="row">
				<div class="col"></div>
				<div class="col-md-5">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="E-mail" name="email">
					</div>
					<div class="form-group">
						<input class="form-control" type="password" placeholder="Password" name="password">
						<?php if(isset($loginErr)){ echo "<br><div class='alert alert-danger'>" . $loginErr . "</div>";}?>
					</div>
					<div class="form-group">
						<input type="submit" name="login" class="btn btn-default" value="Log In"/>
					</div>
					<div class="form-group">
						<p>No account yet? Create one <a href="register.php">here</a>.</p>
					</div>
				</div>
				<div class="col"></div>
			</div>
		</form>
	</div>
</div>
<?php include("includes/end.php"); ?>